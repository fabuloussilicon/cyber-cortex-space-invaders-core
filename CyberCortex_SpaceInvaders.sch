<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="O_HSYNC" />
        <signal name="O_VSYNC" />
        <signal name="O_VIDEO_R(3:0)" />
        <signal name="O_VIDEO_G(3:0)" />
        <signal name="O_VIDEO_B(3:0)" />
        <signal name="O_AUDIO_L" />
        <signal name="O_AUDIO_R" />
        <signal name="I_BUTTON(5:0)" />
        <signal name="I_RESET" />
        <signal name="clk" />
        <signal name="XLXN_12" />
        <port polarity="Output" name="O_HSYNC" />
        <port polarity="Output" name="O_VSYNC" />
        <port polarity="Output" name="O_VIDEO_R(3:0)" />
        <port polarity="Output" name="O_VIDEO_G(3:0)" />
        <port polarity="Output" name="O_VIDEO_B(3:0)" />
        <port polarity="Output" name="O_AUDIO_L" />
        <port polarity="Output" name="O_AUDIO_R" />
        <port polarity="Input" name="I_BUTTON(5:0)" />
        <port polarity="Input" name="I_RESET" />
        <port polarity="Input" name="clk" />
        <blockdef name="invaders_top">
            <timestamp>2014-6-2T21:31:4</timestamp>
            <rect width="64" x="0" y="20" height="24" />
            <line x2="0" y1="32" y2="32" x1="64" />
            <rect width="64" x="432" y="20" height="24" />
            <line x2="496" y1="32" y2="32" x1="432" />
            <rect width="64" x="432" y="84" height="24" />
            <line x2="496" y1="96" y2="96" x1="432" />
            <rect width="64" x="432" y="148" height="24" />
            <line x2="496" y1="160" y2="160" x1="432" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="496" y1="-224" y2="-224" x1="432" />
            <line x2="496" y1="-160" y2="-160" x1="432" />
            <line x2="496" y1="-96" y2="-96" x1="432" />
            <line x2="496" y1="-32" y2="-32" x1="432" />
            <rect width="368" x="64" y="-268" height="460" />
        </blockdef>
        <blockdef name="Clock">
            <timestamp>2014-6-2T21:57:54</timestamp>
            <rect width="336" x="64" y="-256" height="256" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="464" y1="-224" y2="-224" x1="400" />
            <line x2="464" y1="-160" y2="-160" x1="400" />
            <line x2="464" y1="-96" y2="-96" x1="400" />
            <line x2="464" y1="-32" y2="-32" x1="400" />
        </blockdef>
        <block symbolname="invaders_top" name="XLXI_1">
            <blockpin signalname="I_RESET" name="I_RESET" />
            <blockpin signalname="XLXN_12" name="I_CLK_REF" />
            <blockpin signalname="O_HSYNC" name="O_HSYNC" />
            <blockpin signalname="O_VSYNC" name="O_VSYNC" />
            <blockpin signalname="O_AUDIO_L" name="O_AUDIO_L" />
            <blockpin signalname="O_AUDIO_R" name="O_AUDIO_R" />
            <blockpin signalname="I_BUTTON(5:0)" name="I_BUTTON(5:0)" />
            <blockpin signalname="O_VIDEO_R(3:0)" name="O_VIDEO_R(3:0)" />
            <blockpin signalname="O_VIDEO_G(3:0)" name="O_VIDEO_G(3:0)" />
            <blockpin signalname="O_VIDEO_B(3:0)" name="O_VIDEO_B(3:0)" />
        </block>
        <block symbolname="Clock" name="XLXI_2">
            <blockpin signalname="clk" name="CLKIN_IN" />
            <blockpin name="LOCKED_OUT" />
            <blockpin signalname="XLXN_12" name="CLKFX_OUT" />
            <blockpin name="CLKIN_IBUFG_OUT" />
            <blockpin name="CLK0_OUT" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <instance x="1664" y="1568" name="XLXI_1" orien="R0">
        </instance>
        <branch name="O_HSYNC">
            <wire x2="2192" y1="1344" y2="1344" x1="2160" />
        </branch>
        <iomarker fontsize="28" x="2192" y="1344" name="O_HSYNC" orien="R0" />
        <branch name="O_VSYNC">
            <wire x2="2192" y1="1408" y2="1408" x1="2160" />
        </branch>
        <iomarker fontsize="28" x="2192" y="1408" name="O_VSYNC" orien="R0" />
        <branch name="O_VIDEO_R(3:0)">
            <wire x2="2192" y1="1600" y2="1600" x1="2160" />
        </branch>
        <iomarker fontsize="28" x="2192" y="1600" name="O_VIDEO_R(3:0)" orien="R0" />
        <branch name="O_VIDEO_G(3:0)">
            <wire x2="2192" y1="1664" y2="1664" x1="2160" />
        </branch>
        <iomarker fontsize="28" x="2192" y="1664" name="O_VIDEO_G(3:0)" orien="R0" />
        <branch name="O_VIDEO_B(3:0)">
            <wire x2="2192" y1="1728" y2="1728" x1="2160" />
        </branch>
        <iomarker fontsize="28" x="2192" y="1728" name="O_VIDEO_B(3:0)" orien="R0" />
        <branch name="O_AUDIO_L">
            <wire x2="2192" y1="1472" y2="1472" x1="2160" />
        </branch>
        <iomarker fontsize="28" x="2192" y="1472" name="O_AUDIO_L" orien="R0" />
        <branch name="O_AUDIO_R">
            <wire x2="2192" y1="1536" y2="1536" x1="2160" />
        </branch>
        <iomarker fontsize="28" x="2192" y="1536" name="O_AUDIO_R" orien="R0" />
        <branch name="I_BUTTON(5:0)">
            <wire x2="1664" y1="1600" y2="1600" x1="1632" />
        </branch>
        <iomarker fontsize="28" x="1632" y="1600" name="I_BUTTON(5:0)" orien="R180" />
        <branch name="I_RESET">
            <wire x2="1664" y1="1344" y2="1344" x1="1632" />
        </branch>
        <iomarker fontsize="28" x="1632" y="1344" name="I_RESET" orien="R180" />
        <instance x="720" y="1600" name="XLXI_2" orien="R0">
        </instance>
        <branch name="clk">
            <wire x2="720" y1="1376" y2="1376" x1="688" />
        </branch>
        <iomarker fontsize="28" x="688" y="1376" name="clk" orien="R180" />
        <branch name="XLXN_12">
            <wire x2="1424" y1="1440" y2="1440" x1="1184" />
            <wire x2="1424" y1="1440" y2="1536" x1="1424" />
            <wire x2="1664" y1="1536" y2="1536" x1="1424" />
        </branch>
    </sheet>
</drawing>